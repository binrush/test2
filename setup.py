from setuptools import setup

setup(name='test2',
      version='1.0.0',
      description='Test job',
      author='Rushan Shaymardanov',
      author_email='rush.ru@gmail.com',
      install_requires=[
          'psycopg2',
          'Flask==0.10.1',
          'matplotlib']
)
