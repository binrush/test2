import psycopg2
import app
import sys
import gzip
import logging

logging.basicConfig(level=logging.DEBUG)

if len(sys.argv) != 2:
    print("Usage: python prepare-db.py <filename>")
    sys.exit(1)

schema = """
CREATE TABLE stat (
    uid text NOT NULL,
    action char(1) NOT NULL,
    level integer NOT NULL,
    event_time bigint NOT NULL
);"""

epoch_to_date_func = """
CREATE OR REPLACE FUNCTION epoch_to_date(input bigint)
   RETURNS date
   LANGUAGE SQL
   IMMUTABLE
AS
$body$
  SELECT to_timestamp(input/1000)::date;
$body$;
"""

copy_command="COPY stat FROM STDIN (FORMAT 'csv', DELIMITER ';', HEADER);"

dau_view = """CREATE MATERIALIZED VIEW dau AS 
                SELECT COUNT(DISTINCT uid), epoch_to_date(event_time) AS dt 
                FROM stat GROUP BY dt;"""

avg_lvl_time_view = """
CREATE MATERIALIZED VIEW avg_lvl_time AS

SELECT AVG(tm/lvls) avg_time, level, dt FROM (
    SELECT level, epoch_to_date(event_time) dt,
           MAX(event_time) OVER (PARTITION by uid) - MIN(event_time) OVER (PARTITION by uid) tm,
           MAX(level) OVER (PARTITION by uid) - MIN(level) OVER (PARTITION by uid) lvls
    FROM stat 
    WHERE action='c'
) t
WHERE lvls != 0
GROUP BY level, dt
ORDER BY level;
"""

players_levels_view = """
CREATE MATERIALIZED VIEW levels AS

SELECT lvl, dt, COUNT(*) users_count
FROM 
    (SELECT MAX(level) lvl, uid, epoch_to_date(event_time) dt
     FROM stat GROUP BY uid, epoch_to_date(event_time)) t 
GROUP BY lvl, dt;
"""
filename = sys.argv[1]

with psycopg2.connect(app.PG_DSN) as conn, \
        gzip.open(filename) as f:
    with conn.cursor() as cur:
        logging.info("Creating schema")
        cur.execute(schema)
        cur.execute(epoch_to_date_func)
        logging.info("Copying data")
        cur.copy_expert(copy_command, f)
        logging.info("Creating indexes")
        cur.execute("CREATE INDEX ON stat (action)")
        logging.info("Creating dau view")
        cur.execute(dau_view)
        logging.info("Creating averange levels time view")
        cur.execute(avg_lvl_time_view)
        logging.info("Creating levels view")
        cur.execute(players_levels_view)
        conn.commit()
