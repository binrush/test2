from __future__ import division
from flask import Flask, render_template, send_file, request
from psycopg2 import connect
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plot
import matplotlib.dates as mdates
from cStringIO import StringIO
import datetime
import os
import logging

app = Flask(__name__)

PG_DSN="dbname=test2"
DATE_FORMAT="%Y-%m-%d"
APP_ROOT = os.path.dirname(os.path.abspath(__file__))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/dau.png')
def dau_diagram():
    datefrom = datetime.datetime.strptime(request.args['datefrom'], DATE_FORMAT).date()
    dateto = datetime.datetime.strptime(request.args['dateto'], DATE_FORMAT).date()
   
    sql = """SELECT dt, count FROM dau WHERE dt BETWEEN %s AND %s"""
    dates = []
    dau = []
    with connect(PG_DSN) as conn:
        with conn.cursor() as cur:
            cur.execute(sql, (datefrom, dateto))
            if cur.rowcount == 0:
                return send_file(os.path.join(APP_ROOT, 'nodata.png'))
            for row in cur:
                dates.append(row[0])
                dau.append(row[1])

    output = StringIO()
    fig, ax = plot.subplots()
    rects1 = ax.bar(dates, dau)
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(mdates.DateFormatter(DATE_FORMAT))
    fig.autofmt_xdate()
    plot.savefig(output, format='png')
    output.seek(0)
    return send_file(output, 'image/png')


@app.route('/dau')
def dau():
    try:
        datefrom = datetime.datetime.strptime(request.args['datefrom'], DATE_FORMAT).date()
        dateto = datetime.datetime.strptime(request.args['dateto'], DATE_FORMAT).date()
    except (KeyError, ValueError) as e:
        # Showing graph for the whole period
        with connect(PG_DSN) as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT MIN(dt), MAX(dt) FROM dau")
                datefrom, dateto = cur.fetchone()
    return render_template(
        'dau.html',
        datefrom=datefrom.strftime(DATE_FORMAT),
        dateto=dateto.strftime(DATE_FORMAT))


@app.route('/levels.png')
def levels_diagram():
    date = datetime.datetime.strptime(request.args['date'], DATE_FORMAT)
    lvlfrom = int(request.args['levelfrom'])
    lvlto = int(request.args['levelto'])
    sql = """SELECT lvl, users_count FROM levels
             WHERE dt=%s AND lvl BETWEEN %s AND %s"""
    levels = []
    users_count= []
    users_count_all = 0
    with connect(PG_DSN) as conn:
        with conn.cursor() as cur:
            # Count all users
            cur.execute("SELECT SUM(users_count) FROM levels WHERE dt=%s", (date, ))
            users_count_all = cur.fetchone()[0]
            cur.execute(sql, (date, lvlfrom, lvlto))
            for row in cur:
                levels.append(row[0])
                users_count.append(row[1])
    output = StringIO()
    fig, ax = plot.subplots()
    rects1 = ax.bar(levels, [100*uc/users_count_all for uc in users_count])
    plot.savefig(output, format='png')
    output.seek(0)
    return send_file(output, 'image/png')


@app.route('/levels')
def levels():
    try:
        date = datetime.datetime.strptime(request.args['date'], DATE_FORMAT).date()
        levelfrom = int(request.args['levelfrom'])
        levelto = int(request.args['levelto'])
    except (KeyError, ValueError) as e:
        with connect(PG_DSN) as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT MAX(dt) from levels")
                date = cur.fetchone()[0]
                cur.execute("SELECT MIN(lvl), MAX(lvl) FROM levels WHERE dt=%s", (date, ))
                levelfrom, levelto = cur.fetchone()
    return render_template(
        'levels.html',
        date=date.strftime(DATE_FORMAT),
        levelfrom=levelfrom,
        levelto=levelto)


@app.route('/avgtime.png')
def avgtime_diagram():
    datefrom = datetime.datetime.strptime(request.args['datefrom'], DATE_FORMAT)
    dateto = datetime.datetime.strptime(request.args['dateto'], DATE_FORMAT)
    lvlfrom = int(request.args['levelfrom'])
    lvlto = int(request.args['levelto'])
    sql = """SELECT level, avg_time FROM avg_lvl_time
             WHERE dt BETWEEN %s AND %s
                AND level BETWEEN %s AND %s"""
    levels = []
    avg_time = []
    with connect(PG_DSN) as conn:
        with conn.cursor() as cur:
            cur.execute(sql, (datefrom, dateto, lvlfrom, lvlto))
            for row in cur:
                levels.append(row[0])
                avg_time.append(row[1])
    output = StringIO()
    fig, ax = plot.subplots()
    rects1 = ax.bar(levels, [ t/60000 for t in avg_time])
    plot.savefig(output, format='png')
    output.seek(0)
    return send_file(output, 'image/png')


@app.route('/avgtime')
def avgtime():
    try:
        datefrom = datetime.datetime.strptime(request.args['datefrom'], DATE_FORMAT).date()
        dateto = datetime.datetime.strptime(request.args['dateto'], DATE_FORMAT).date()
        levelfrom = int(request.args['levelfrom'])
        levelto = int(request.args['levelto'])
    except (KeyError, ValueError) as e:
        with connect(PG_DSN) as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT MIN(dt), MAX(dt) from avg_lvl_time")
                datefrom, dateto = cur.fetchone()
                cur.execute("SELECT MIN(level), MAX(level) FROM avg_lvl_time")
                levelfrom, levelto = cur.fetchone()
    return render_template(
        'avgtime.html',
        datefrom=datefrom.strftime(DATE_FORMAT),
        dateto=dateto.strftime(DATE_FORMAT),
        levelfrom=levelfrom,
        levelto=levelto)


if __name__ == '__main__':
    app.run(debug=True)

